api = 2
core = 8.x

; Modules

projects[betterlogin][version] = "1.5"

projects[config_update][version] = "1.7"

projects[ctools][version] = "3.7"

projects[default_content][version] = "1.0-alpha9"

projects[entity_reference_revisions][version] = "1.9"

projects[features][version] =  "3.12"

projects[field_group][version] = "3.1"

projects[file_entity][version] = "2.0-beta9"

projects[fontawesome][version] = "2.19"

projects[geolocation][version] = "1.12"

projects[jquery_ui][version] = "1.4"

projects[jquery_ui_draggable][version] = "1.2"

projects[jquery_ui_droppable][version] = "1.2"

projects[page_manager][version] = "4.0-beta6"

projects[panelizer][version] = "4.4"

projects[panels][version] = "4.6"

projects[paragraphs][version] = "1.12"

projects[token][version] = "1.9"

projects[twig_field_value][version] = "1.2"

projects[video_embed_field][version] = "2.4"

projects[webform][version] = "5.27"
