webpackJsonp([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });var select = function select(elem) {
  return document.querySelector(elem);
};

var selectAll = function selectAll(elem) {
  return document.querySelectorAll(elem);
};

var toggleClass = function toggleClass(target, className) {

  return function (e) {
    e.preventDefault();
    target.classList.toggle(className);
  };
};exports.

select = select;exports.selectAll = selectAll;exports.toggleClass = toggleClass;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";



__webpack_require__(2);


__webpack_require__(3);


__webpack_require__(4);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var _smoothscrollPolyfill = __webpack_require__(5);var _smoothscrollPolyfill2 = _interopRequireDefault(_smoothscrollPolyfill);
var _headerVeiws = __webpack_require__(6);var _headerVeiws2 = _interopRequireDefault(_headerVeiws);
var _arrowDown = __webpack_require__(7);var _arrowDown2 = _interopRequireDefault(_arrowDown);
var _more = __webpack_require__(8);var _more2 = _interopRequireDefault(_more);
var _anchor = __webpack_require__(9);var _anchor2 = _interopRequireDefault(_anchor);
var _objectFitIe = __webpack_require__(10);var _objectFitIe2 = _interopRequireDefault(_objectFitIe);
var _gallerySlider = __webpack_require__(11);var _gallerySlider2 = _interopRequireDefault(_gallerySlider);
var _submit = __webpack_require__(12);var _submit2 = _interopRequireDefault(_submit);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

(function ($) {
  _smoothscrollPolyfill2.default.polyfill();
  (0, _objectFitIe2.default)();
  (0, _headerVeiws2.default)();
  (0, _arrowDown2.default)();
  (0, _more2.default)();
  (0, _anchor2.default)();
  (0, _gallerySlider2.default)();
  //submit();
})(jQuery);

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

/* smoothscroll v0.4.0 - 2018 - Dustan Kasten, Jeremias Menichelli - MIT License */
(function () {
  'use strict';

  // polyfill
  function polyfill() {
    // aliases
    var w = window;
    var d = document;

    // return if scroll behavior is supported and polyfill is not forced
    if (
      'scrollBehavior' in d.documentElement.style &&
      w.__forceSmoothScrollPolyfill__ !== true
    ) {
      return;
    }

    // globals
    var Element = w.HTMLElement || w.Element;
    var SCROLL_TIME = 468;

    // object gathering original scroll methods
    var original = {
      scroll: w.scroll || w.scrollTo,
      scrollBy: w.scrollBy,
      elementScroll: Element.prototype.scroll || scrollElement,
      scrollIntoView: Element.prototype.scrollIntoView
    };

    // define timing method
    var now =
      w.performance && w.performance.now
        ? w.performance.now.bind(w.performance)
        : Date.now;

    /**
     * indicates if a the current browser is made by Microsoft
     * @method isMicrosoftBrowser
     * @param {String} userAgent
     * @returns {Boolean}
     */
    function isMicrosoftBrowser(userAgent) {
      var userAgentPatterns = ['MSIE ', 'Trident/', 'Edge/'];

      return new RegExp(userAgentPatterns.join('|')).test(userAgent);
    }

    /*
     * IE has rounding bug rounding down clientHeight and clientWidth and
     * rounding up scrollHeight and scrollWidth causing false positives
     * on hasScrollableSpace
     */
    var ROUNDING_TOLERANCE = isMicrosoftBrowser(w.navigator.userAgent) ? 1 : 0;

    /**
     * changes scroll position inside an element
     * @method scrollElement
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    function scrollElement(x, y) {
      this.scrollLeft = x;
      this.scrollTop = y;
    }

    /**
     * returns result of applying ease math function to a number
     * @method ease
     * @param {Number} k
     * @returns {Number}
     */
    function ease(k) {
      return 0.5 * (1 - Math.cos(Math.PI * k));
    }

    /**
     * indicates if a smooth behavior should be applied
     * @method shouldBailOut
     * @param {Number|Object} firstArg
     * @returns {Boolean}
     */
    function shouldBailOut(firstArg) {
      if (
        firstArg === null ||
        typeof firstArg !== 'object' ||
        firstArg.behavior === undefined ||
        firstArg.behavior === 'auto' ||
        firstArg.behavior === 'instant'
      ) {
        // first argument is not an object/null
        // or behavior is auto, instant or undefined
        return true;
      }

      if (typeof firstArg === 'object' && firstArg.behavior === 'smooth') {
        // first argument is an object and behavior is smooth
        return false;
      }

      // throw error when behavior is not supported
      throw new TypeError(
        'behavior member of ScrollOptions ' +
          firstArg.behavior +
          ' is not a valid value for enumeration ScrollBehavior.'
      );
    }

    /**
     * indicates if an element has scrollable space in the provided axis
     * @method hasScrollableSpace
     * @param {Node} el
     * @param {String} axis
     * @returns {Boolean}
     */
    function hasScrollableSpace(el, axis) {
      if (axis === 'Y') {
        return el.clientHeight + ROUNDING_TOLERANCE < el.scrollHeight;
      }

      if (axis === 'X') {
        return el.clientWidth + ROUNDING_TOLERANCE < el.scrollWidth;
      }
    }

    /**
     * indicates if an element has a scrollable overflow property in the axis
     * @method canOverflow
     * @param {Node} el
     * @param {String} axis
     * @returns {Boolean}
     */
    function canOverflow(el, axis) {
      var overflowValue = w.getComputedStyle(el, null)['overflow' + axis];

      return overflowValue === 'auto' || overflowValue === 'scroll';
    }

    /**
     * indicates if an element can be scrolled in either axis
     * @method isScrollable
     * @param {Node} el
     * @param {String} axis
     * @returns {Boolean}
     */
    function isScrollable(el) {
      var isScrollableY = hasScrollableSpace(el, 'Y') && canOverflow(el, 'Y');
      var isScrollableX = hasScrollableSpace(el, 'X') && canOverflow(el, 'X');

      return isScrollableY || isScrollableX;
    }

    /**
     * finds scrollable parent of an element
     * @method findScrollableParent
     * @param {Node} el
     * @returns {Node} el
     */
    function findScrollableParent(el) {
      var isBody;

      do {
        el = el.parentNode;

        isBody = el === d.body;
      } while (isBody === false && isScrollable(el) === false);

      isBody = null;

      return el;
    }

    /**
     * self invoked function that, given a context, steps through scrolling
     * @method step
     * @param {Object} context
     * @returns {undefined}
     */
    function step(context) {
      var time = now();
      var value;
      var currentX;
      var currentY;
      var elapsed = (time - context.startTime) / SCROLL_TIME;

      // avoid elapsed times higher than one
      elapsed = elapsed > 1 ? 1 : elapsed;

      // apply easing to elapsed time
      value = ease(elapsed);

      currentX = context.startX + (context.x - context.startX) * value;
      currentY = context.startY + (context.y - context.startY) * value;

      context.method.call(context.scrollable, currentX, currentY);

      // scroll more if we have not reached our destination
      if (currentX !== context.x || currentY !== context.y) {
        w.requestAnimationFrame(step.bind(w, context));
      }
    }

    /**
     * scrolls window or element with a smooth behavior
     * @method smoothScroll
     * @param {Object|Node} el
     * @param {Number} x
     * @param {Number} y
     * @returns {undefined}
     */
    function smoothScroll(el, x, y) {
      var scrollable;
      var startX;
      var startY;
      var method;
      var startTime = now();

      // define scroll context
      if (el === d.body) {
        scrollable = w;
        startX = w.scrollX || w.pageXOffset;
        startY = w.scrollY || w.pageYOffset;
        method = original.scroll;
      } else {
        scrollable = el;
        startX = el.scrollLeft;
        startY = el.scrollTop;
        method = scrollElement;
      }

      // scroll looping over a frame
      step({
        scrollable: scrollable,
        method: method,
        startTime: startTime,
        startX: startX,
        startY: startY,
        x: x,
        y: y
      });
    }

    // ORIGINAL METHODS OVERRIDES
    // w.scroll and w.scrollTo
    w.scroll = w.scrollTo = function() {
      // avoid action when no arguments are passed
      if (arguments[0] === undefined) {
        return;
      }

      // avoid smooth behavior if not required
      if (shouldBailOut(arguments[0]) === true) {
        original.scroll.call(
          w,
          arguments[0].left !== undefined
            ? arguments[0].left
            : typeof arguments[0] !== 'object'
              ? arguments[0]
              : w.scrollX || w.pageXOffset,
          // use top prop, second argument if present or fallback to scrollY
          arguments[0].top !== undefined
            ? arguments[0].top
            : arguments[1] !== undefined
              ? arguments[1]
              : w.scrollY || w.pageYOffset
        );

        return;
      }

      // LET THE SMOOTHNESS BEGIN!
      smoothScroll.call(
        w,
        d.body,
        arguments[0].left !== undefined
          ? ~~arguments[0].left
          : w.scrollX || w.pageXOffset,
        arguments[0].top !== undefined
          ? ~~arguments[0].top
          : w.scrollY || w.pageYOffset
      );
    };

    // w.scrollBy
    w.scrollBy = function() {
      // avoid action when no arguments are passed
      if (arguments[0] === undefined) {
        return;
      }

      // avoid smooth behavior if not required
      if (shouldBailOut(arguments[0])) {
        original.scrollBy.call(
          w,
          arguments[0].left !== undefined
            ? arguments[0].left
            : typeof arguments[0] !== 'object' ? arguments[0] : 0,
          arguments[0].top !== undefined
            ? arguments[0].top
            : arguments[1] !== undefined ? arguments[1] : 0
        );

        return;
      }

      // LET THE SMOOTHNESS BEGIN!
      smoothScroll.call(
        w,
        d.body,
        ~~arguments[0].left + (w.scrollX || w.pageXOffset),
        ~~arguments[0].top + (w.scrollY || w.pageYOffset)
      );
    };

    // Element.prototype.scroll and Element.prototype.scrollTo
    Element.prototype.scroll = Element.prototype.scrollTo = function() {
      // avoid action when no arguments are passed
      if (arguments[0] === undefined) {
        return;
      }

      // avoid smooth behavior if not required
      if (shouldBailOut(arguments[0]) === true) {
        // if one number is passed, throw error to match Firefox implementation
        if (typeof arguments[0] === 'number' && arguments[1] === undefined) {
          throw new SyntaxError('Value could not be converted');
        }

        original.elementScroll.call(
          this,
          // use left prop, first number argument or fallback to scrollLeft
          arguments[0].left !== undefined
            ? ~~arguments[0].left
            : typeof arguments[0] !== 'object' ? ~~arguments[0] : this.scrollLeft,
          // use top prop, second argument or fallback to scrollTop
          arguments[0].top !== undefined
            ? ~~arguments[0].top
            : arguments[1] !== undefined ? ~~arguments[1] : this.scrollTop
        );

        return;
      }

      var left = arguments[0].left;
      var top = arguments[0].top;

      // LET THE SMOOTHNESS BEGIN!
      smoothScroll.call(
        this,
        this,
        typeof left === 'undefined' ? this.scrollLeft : ~~left,
        typeof top === 'undefined' ? this.scrollTop : ~~top
      );
    };

    // Element.prototype.scrollBy
    Element.prototype.scrollBy = function() {
      // avoid action when no arguments are passed
      if (arguments[0] === undefined) {
        return;
      }

      // avoid smooth behavior if not required
      if (shouldBailOut(arguments[0]) === true) {
        original.elementScroll.call(
          this,
          arguments[0].left !== undefined
            ? ~~arguments[0].left + this.scrollLeft
            : ~~arguments[0] + this.scrollLeft,
          arguments[0].top !== undefined
            ? ~~arguments[0].top + this.scrollTop
            : ~~arguments[1] + this.scrollTop
        );

        return;
      }

      this.scroll({
        left: ~~arguments[0].left + this.scrollLeft,
        top: ~~arguments[0].top + this.scrollTop,
        behavior: arguments[0].behavior
      });
    };

    // Element.prototype.scrollIntoView
    Element.prototype.scrollIntoView = function() {
      // avoid smooth behavior if not required
      if (shouldBailOut(arguments[0]) === true) {
        original.scrollIntoView.call(
          this,
          arguments[0] === undefined ? true : arguments[0]
        );

        return;
      }

      // LET THE SMOOTHNESS BEGIN!
      var scrollableParent = findScrollableParent(this);
      var parentRects = scrollableParent.getBoundingClientRect();
      var clientRects = this.getBoundingClientRect();

      if (scrollableParent !== d.body) {
        // reveal element inside parent
        smoothScroll.call(
          this,
          scrollableParent,
          scrollableParent.scrollLeft + clientRects.left - parentRects.left,
          scrollableParent.scrollTop + clientRects.top - parentRects.top
        );

        // reveal parent in viewport unless is fixed
        if (w.getComputedStyle(scrollableParent).position !== 'fixed') {
          w.scrollBy({
            left: parentRects.left,
            top: parentRects.top,
            behavior: 'smooth'
          });
        }
      } else {
        // reveal element in viewport
        w.scrollBy({
          left: clientRects.left,
          top: clientRects.top,
          behavior: 'smooth'
        });
      }
    };
  }

  if (true) {
    // commonjs
    module.exports = { polyfill: polyfill };
  } else {
    // global
    polyfill();
  }

}());


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

headerVeiws;var _helpers = __webpack_require__(0);function headerVeiws() {
  var header = (0, _helpers.select)('header.header');
  var humburger = (0, _helpers.select)('.humburger');

  if (header !== null && !(0, _helpers.select)('body').classList.contains('typography')) {
    //
    // header--sticky
    //
    document.addEventListener('scroll', function () {

      if (window.pageYOffset > 0) {
        header.classList.add('header--sticky');
      } else {
        header.classList.remove('header--sticky');
      }
    });

    //
    // header--mobile
    //
    humburger.addEventListener('click', function () {
      this.classList.toggle('humburger--active');

      if (header.classList.contains('header--mobile')) {
        header.classList.remove('header--mobile');
      } else {
        header.classList.add('header--mobile');
      }
    });
  }
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

arrowDown;var _helpers = __webpack_require__(0);function arrowDown() {

  var scrollToProduct = function scrollToProduct(e) {
    e.preventDefault();

    var product = (0, _helpers.select)('.product');
    if (product !== null) {
      var productHeight = product.offsetHeight;
      window.scroll({
        top: productHeight - 136,
        left: 0,
        behavior: 'smooth' });

    }
  };

  (0, _helpers.select)('.arrow').addEventListener('click', scrollToProduct);
}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

more;var _helpers = __webpack_require__(0);function more() {
  var moreBtns = (0, _helpers.selectAll)('.more');

  var toggleMore = function toggleMore(e) {
    e.preventDefault();
    var moreWrap = this.nextElementSibling;
    var moreWrapHeight = getComputedStyle(moreWrap).height;
    var moreContent = this.nextElementSibling.firstElementChild;
    var moreContentHeight = getComputedStyle(moreContent).height;

    if (moreWrapHeight === '0px') {
      moreWrap.style.height = moreContentHeight;
      this.classList.toggle('more--opened');
    } else {
      moreWrap.style.height = '';
      this.classList.toggle('more--opened');
    }
  };

  for (var i = 0; i < moreBtns.length; ++i) {
    moreBtns[i].addEventListener('click', toggleMore);
    moreBtns[i].addEventListener('touchend', toggleMore);
    moreBtns[i].addEventListener('mouseleave', (0, _helpers.toggleClass)(moreBtns[i], 'has-hover'));
    moreBtns[i].addEventListener('mouseover', (0, _helpers.toggleClass)(moreBtns[i], 'has-hover'));
  }
}

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

anchor;var _helpers = __webpack_require__(0);function anchor() {
  var header = (0, _helpers.select)('header.header');
  var menuLinks = (0, _helpers.selectAll)('header.header .nav__link');

  var scrollToSection = function scrollToSection(e) {
    var target = e.target.closest('a[href^="#"]:not([href$="#"])');
    if (!target) return;
    var anchor = target.getAttribute('href').split('#')[1];
    var section = (0, _helpers.select)('#' + anchor);
    if (!section) return;
    var sectionCoords = section.getBoundingClientRect().top + pageYOffset;
    e.preventDefault();
    window.scroll({
      top: sectionCoords - 136,
      left: 0,
      behavior: 'smooth' });

    header.classList.remove('header--mobile');

    if (history.pushState) {
      history.pushState(null, null, '#' + anchor);
    } else {
      location.hash = '#' + anchor;
      scrollTo(0, sectionCoords - 136);
    }
  };

  document.addEventListener('click', scrollToSection);

  document.onscroll = function () {
    for (var i = 0; i < menuLinks.length; ++i) {
      if (!(0, _helpers.select)('body').classList.contains('typography')) {
        menuLinks[i].classList.remove('nav__link--active');
      }
      var _anchor = menuLinks[i].getAttribute('href').split('#')[1];
      var section = (0, _helpers.select)('#' + _anchor);
      var sectionCoords = section.getBoundingClientRect().top + pageYOffset;

      if (section !== null) {
        var sectionTop = sectionCoords - 200;
        var sectionHeight = section.offsetHeight;

        if (sectionTop <= pageYOffset && sectionTop + sectionHeight > pageYOffset) {
          menuLinks[i].classList.add('nav__link--active');
        }
      }
    }
  };
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

objectFitIE;var _helpers = __webpack_require__(0);function objectFitIE() {

  if ('objectFit' in document.documentElement.style === false) {
    document.addEventListener('DOMContentLoaded', function () {
      Array.prototype.forEach.call((0, _helpers.selectAll)('.js-image-wrap'), function (imageWrap) {
        var imgUrl = imageWrap.querySelector('img').getAttribute("src");

        if (imgUrl) {
          imageWrap.style.backgroundImage = 'url(' + imgUrl + ')';
          imageWrap.classList.add("custom-object-fit");
        }
      });
    });
  }
}

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

gallerySlider;var _helpers = __webpack_require__(0);function gallerySlider() {

  //
  // Slider: initialize
  //

  jQuery('.gallery-slider__for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.gallery-slider__nav',
    infinite: false });


  jQuery('.gallery-slider__nav').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.gallery-slider__for',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    infinite: false });


  jQuery('.gallery-slider--mobile').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    asNavFor: '.gallery-slider__nav' });


  var slider = (0, _helpers.select)('.gallery-slider');

  if (slider !== null) {

    //
    // Slider: setting open/close
    //

    var images = (0, _helpers.selectAll)('.gallery__image');

    for (var i = 0; i < images.length; ++i) {
      images[i].addEventListener('click', (0, _helpers.toggleClass)(slider, 'opened'));
    }


    //
    // Slider: custom features
    //

    var sliderNavImages = (0, _helpers.selectAll)('.gallery-slider__nav-item');

    for (var _i = 0; _i < sliderNavImages.length; ++_i) {
      sliderNavImages[_i].setAttribute('data-position', _i + 1);
    }

    var sliderActiveElems = (0, _helpers.selectAll)('.slick-arrow, .js-image-wrap');

    var refreshSliderInfo = function refreshSliderInfo() {
      var sliderCurrentItem = (0, _helpers.select)('.gallery-slider__nav-item.slick-current');
      var alt = sliderCurrentItem.querySelector('img').getAttribute('alt');
      var sliderAmounts = (0, _helpers.selectAll)('.gallery-slider__amount');
      var sliderTitleImages = (0, _helpers.selectAll)('.gallery-slider__title');
      var sliderMaxPosition = sliderNavImages[sliderNavImages.length - 1].getAttribute('data-position');
      var sliderAmountInner = sliderCurrentItem.getAttribute('data-position') + '/' + sliderMaxPosition;

      for (var _i2 = 0; _i2 < sliderAmounts.length; ++_i2) {
        sliderAmounts[_i2].innerHTML = sliderAmountInner;
      }

      for (var _i3 = 0; _i3 < sliderTitleImages.length; ++_i3) {
        sliderTitleImages[_i3].innerHTML = alt;
      }
    };

    refreshSliderInfo();

    for (var _i4 = 0; _i4 < sliderActiveElems.length; ++_i4) {
      sliderActiveElems[_i4].addEventListener('click', refreshSliderInfo);
    }

    jQuery('.gallery-slider__for, .gallery-slider--mobile').on('swipe', refreshSliderInfo);
  }

  slider.addEventListener('click', function (e) {
    e.preventDefault();
    var sliderContent = e.target.closest('.gallery-slider__content');
    if (sliderContent) return;
    slider.classList.remove('opened');
  });

  slider.addEventListener('mouseover', function (e) {
    e.preventDefault();
    var sliderContent = e.target.closest('.gallery-slider__content');
    if (sliderContent) {
      (0, _helpers.select)('.gallery-slider__close').classList.remove('hover');
    } else {
      (0, _helpers.select)('.gallery-slider__close').classList.add('hover');
    }
  });
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default =

submit;var _helpers = __webpack_require__(0);function submit() {
  var agentForm = (0, _helpers.select)('.contact-agent__agent-form');
  var form = (0, _helpers.select)('.contact-agent__agent-form form');
  var tryAgain = (0, _helpers.select)('.great__try-again');

  if (agentForm !== null) {
    form.addEventListener('submit', (0, _helpers.toggleClass)(agentForm, 'hidden'));
    tryAgain.addEventListener('click', (0, _helpers.toggleClass)(agentForm, 'hidden'));
  }
}

/***/ })
],[1]);
//# sourceMappingURL=main.js.map